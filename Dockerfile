FROM node:20.9.0-alpine3.18

WORKDIR /home/node/app

COPY . .

RUN yarn install

RUN yarn build

EXPOSE 9000

CMD ["npm start"]

